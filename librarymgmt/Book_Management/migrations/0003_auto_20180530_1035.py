# Generated by Django 2.0 on 2018-05-30 05:05

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('library_management', '0003_auto_20180529_1836'),
        ('Book_Management', '0002_auto_20180529_1836'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Employee',
            new_name='Bookdetails',
        ),
        migrations.RemoveField(
            model_name='assignbook',
            name='bookname',
        ),
        migrations.AddField(
            model_name='assignbook',
            name='BookName',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='library_management.Book'),
        ),
        migrations.AlterField(
            model_name='assignbook',
            name='employeename',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='Employee_Registration.Employee'),
        ),
        migrations.AlterField(
            model_name='bookdetails',
            name='assignbook',
            field=models.ManyToManyField(to='library_management.Book'),
        ),
        migrations.DeleteModel(
            name='Bookname',
        ),
        migrations.DeleteModel(
            name='Employeename',
        ),
    ]
