# Generated by Django 2.0 on 2018-05-30 06:42

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Book_Management', '0004_auto_20180530_1154'),
    ]

    operations = [
        migrations.RenameField(
            model_name='assignbook',
            old_name='BookId',
            new_name='BookName',
        ),
    ]
