from django.contrib import admin


from .models import Assignbook

class AssignAdmin(admin.ModelAdmin):
	list_display = ('employeename', 'BookName', 'issue', 'renew')

admin.site.register(Assignbook, AssignAdmin)