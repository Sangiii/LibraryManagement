# Generated by Django 2.0 on 2018-05-29 12:54

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('library_management', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='admin',
            name='Books',
        ),
        migrations.RenameField(
            model_name='books',
            old_name='author',
            new_name='Author',
        ),
        migrations.RenameField(
            model_name='books',
            old_name='bookId',
            new_name='BookId',
        ),
        migrations.RenameField(
            model_name='books',
            old_name='bookname',
            new_name='BookName',
        ),
        migrations.RenameField(
            model_name='books',
            old_name='publication',
            new_name='Publication',
        ),
        migrations.RenameField(
            model_name='books',
            old_name='published',
            new_name='Published',
        ),
        migrations.AlterField(
            model_name='books',
            name='Types',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='library_management.BookType'),
        ),
        migrations.DeleteModel(
            name='Admin',
        ),
    ]
